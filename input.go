package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"path/filepath"

	"github.com/BurntSushi/toml"
)

const (
	defaultConfigPath = "/etc/shavit/config.toml"
)

const (
	configFlagUsage = "A custom path to the server configuration file"
)

// Flags contain all the flags that were passed to the program
type Flags struct {
	ConfigFile string
}

func getFlags() (Flags, error) {
	var f Flags

	flag.StringVar(&f.ConfigFile, "config", defaultConfigPath, configFlagUsage)
	flag.Parse()

	return f, nil
}

// Config holds the main configuration data for the server
type Config struct {
	// can be relative or absolute path
	SourceDir string `toml:"source"`

	// default to ["index.gmi"]
	IndexFiles []string `toml:"index_files"`

	TLSCert string `toml:"tls_certificate"`
	TLSKey  string `toml:"tls_key"`
}

func getConfig(path string) (Config, error) {
	raw, err := ioutil.ReadFile(path)
	if err != nil {
		return Config{}, fmt.Errorf("failed to read config file: %v", err)
	}

	var cfg Config
	err = toml.Unmarshal(raw, &cfg)
	if err != nil {
		return cfg, fmt.Errorf("failed to parse config file: %v", err)
	}

	cfg.SourceDir, err = filepath.Abs(cfg.SourceDir)
	if err != nil {
		return cfg, fmt.Errorf("failed to get absolute source dir: %v", err)
	}

	if len(cfg.IndexFiles) == 0 {
		cfg.IndexFiles = []string{"index.gmi"}
	}

	return cfg, nil
}
