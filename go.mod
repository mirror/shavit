module git.sr.ht/~yotam/shavit

go 1.12

require (
	git.sr.ht/~yotam/go-gemini v0.0.0-20191109201753-fe15cf054c37
	github.com/BurntSushi/toml v0.3.1
	github.com/pelletier/go-toml v1.6.0 // indirect
)
