package main

import (
	"log"
	"mime"

	gemini "git.sr.ht/~yotam/go-gemini"
)

func main() {
	// gmi is not recognised by Go mime package so we have to manually add it
	err := mime.AddExtensionType(".gmi", "text/gemini")
	if err != nil {
		log.Fatal(err)
	}

	flags, err := getFlags()
	if err != nil {
		log.Fatal(err)
	}

	cfg, err := getConfig(flags.ConfigFile)
	if err != nil {
		log.Fatal(err)
	}

	handler := LoggingHandler{RecovererHandler{Handler{cfg}}}
	err = gemini.ListenAndServe("", cfg.TLSCert, cfg.TLSKey, handler)
	if err != nil {
		log.Fatal(err)
	}
}
